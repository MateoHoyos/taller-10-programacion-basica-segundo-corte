#SIMULADOR SEMI PARABOLICO
'''
def calcularDatosTiroSemiParabolico(Vo,Yo):
    
    t=0
    g=-9.8
    y=0
    
    while y>=0:
        x=Vo*t
        y=Yo+(1/2)*g*(pow(t,2))
        print ("X =", x,"Y=", y)
        t+=0.2

calcularDatosTiroSemiParabolico(36,9)

'''

from graphics import *

ventana= GraphWin ("Simulador Semi Parabólico",1280,720)
ventana.setCoords(0,0,2000,1000)
texto=Entry(Point(50,100),20)

t=0
g=-9.8
y=0
Vo=100
Yo=900

while y>=0:
    x=Vo*t
    y=Yo+(1/2)*g*(pow(t,2))
    print ("X =", x,"Y=", y)
    
    circulo=Circle(Point(x,y) , 3)
    circulo.setFill("Green")
    circulo.draw(ventana)
    
    t+=0.01
print("Tiempo = ", t)

ventana.getMouse()

ventana.close()

