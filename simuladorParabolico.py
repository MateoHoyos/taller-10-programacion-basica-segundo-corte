from math import *
from graphics import *


ventana= GraphWin ("Simulador Parabólico",1280,720)
ventana.setCoords(0,0,2000,1000)

texto=Entry(Point(50,100),20)

t=0
g=9.8
y=0
Vo=100
a=radians(45)   

while y>=0:
        
        x=Vo*cos(a)*t
        y=Vo*sin(a)*t -(1/2)*g*pow(t,2)
        print ("X =", x,"Y=", y)
        circulo=Circle(Point(x,y) , 2)
        circulo.setFill("Red")
        circulo.draw(ventana)
        t+=0.01
print("Tiempo = ", t)




ventana.getMouse()
ventana.close()





